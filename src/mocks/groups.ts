import { GroupDto } from 'src/dto/group.dto';

export const groups: GroupDto[] = [
  {
    id: 1,
    text: 'Группа 1',
  },
  {
    id: 2,
    text: 'Группа 2',
  },
  {
    id: 3,
    text: 'Группа 3',
  },
  {
    id: 4,
    text: 'Группа 4',
  },
];
