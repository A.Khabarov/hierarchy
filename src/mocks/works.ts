import { WorkDto } from 'src/dto/work.dto';

export const works: WorkDto[] = [
  {
    id: 1,
    groupId: 1,
    subgroupId: 1,
    text: 'Работа 1',
  },
  {
    id: 2,
    groupId: 1,
    subgroupId: 1,
    text: 'Работа 2',
  },
  {
    id: 3,
    groupId: 2,
    subgroupId: 2,
    text: 'Работа 4',
  },
  {
    id: 4,
    groupId: 2,
    subgroupId: 2,
    text: 'Работа 4',
  },
  {
    id: 5,
    groupId: 3,
    subgroupId: 3,
    text: 'Работа 5',
  },
  {
    id: 6,
    groupId: 3,
    subgroupId: 3,
    text: 'Работа 6',
  },
  {
    id: 7,
    groupId: 4,
    subgroupId: 4,
    text: 'Работа 7',
  },
  {
    id: 8,
    groupId: 4,
    subgroupId: 4,
    text: 'Работа 8',
  },
];
