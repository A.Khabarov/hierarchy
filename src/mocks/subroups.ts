import { SubgroupDto } from 'src/dto/subgroup.dto';

export const subgroups: SubgroupDto[] = [
  {
    id: 1,
    groupId: 1,
    text: 'Подгруппа 1',
  },
  {
    id: 2,
    groupId: 2,
    text: 'Подгруппа 2',
  },
  {
    id: 3,
    groupId: 3,
    text: 'Подгруппа 3',
  },
  {
    id: 4,
    groupId: 4,
    text: 'Подгруппа 4',
  },
];
