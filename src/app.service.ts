import { Injectable } from '@nestjs/common';
import { groups } from './mocks/groups';
import { subgroups } from './mocks/subroups';
import { works } from './mocks/works';
import { GroupDto } from './dto/group.dto';
import { SubgroupDto } from './dto/subgroup.dto';
import { WorkDto } from './dto/work.dto';
import { AllElementsType } from './types';

@Injectable()
export class AppService {
  getGroups(): GroupDto[] {
    return groups;
  }

  getSubgroups(): SubgroupDto[] {
    return subgroups;
  }

  getWorks(): WorkDto[] {
    return works;
  }

  getAll(): AllElementsType {
    return {
      groups: this.getGroups(),
      subgroups: this.getSubgroups(),
      works: this.getWorks(),
    };
  }
}
