import { GroupDto } from 'src/dto/group.dto';
import { SubgroupDto } from 'src/dto/subgroup.dto';
import { WorkDto } from 'src/dto/work.dto';

export interface AllElementsType {
  groups: GroupDto[];
  subgroups: SubgroupDto[];
  works: WorkDto[];
}
