import { Controller, Get, Query } from '@nestjs/common';
import { ApiQuery, ApiResponse } from '@nestjs/swagger';
import { AppService } from './app.service';
import { GroupDto } from './dto/group.dto';
import { HierarchyDto } from './dto/hierarchy.dto';
import { SubgroupDto } from './dto/subgroup.dto';
import { WorkDto } from './dto/work.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @ApiResponse({
    description: 'get all groups',
    type: [GroupDto],
  })
  @Get('groups')
  getGroups(): GroupDto[] {
    return this.appService.getGroups();
  }

  @ApiResponse({
    description: 'get all subgroups',
    type: [SubgroupDto],
  })
  @ApiQuery({
    required: false,
    name: 'groupId',
  })
  @Get('subgroups')
  getSubgroupsByGroupId(@Query('groupId') id): SubgroupDto[] {
    const subgroups = this.appService.getSubgroups();

    if (id) return subgroups.filter((item) => item.groupId === Number(id));

    return subgroups;
  }

  @ApiResponse({
    description: 'get all works',
    type: [WorkDto],
  })
  @ApiQuery({
    required: false,
    name: 'subgroupId',
  })
  @Get('works')
  getWorks(@Query('subgroupId') id): WorkDto[] {
    const works = this.appService.getWorks();

    if (id) return works.filter((item) => item.subgroupId === Number(id));

    return works;
  }

  @ApiResponse({
    description: 'get hierarchy',
    type: [HierarchyDto],
  })
  @Get('hierarchy')
  getHierarchy(): HierarchyDto[] {
    const { groups, subgroups, works } = this.appService.getAll();

    const mappedSubgroups = subgroups.map((subgroup) => {
      const mappedWorks = works.filter(
        (work) => subgroup.id === work.subgroupId,
      );

      return { ...subgroup, works: mappedWorks };
    });

    const mappedGroups = groups.map((group) => {
      const mappedSubWithWorks = mappedSubgroups.filter(
        (subgroup) => group.id === subgroup.groupId,
      );

      return { ...group, subgroups: mappedSubWithWorks };
    });

    return mappedGroups;
  }
}
