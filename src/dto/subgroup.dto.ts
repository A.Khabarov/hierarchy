import { ApiProperty } from '@nestjs/swagger';

export class SubgroupDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  groupId: number;
  @ApiProperty()
  text: string;
}
