import { ApiProperty } from '@nestjs/swagger';

export class WorkType {
  @ApiProperty()
  id: number;
  @ApiProperty()
  groupId: number;
  @ApiProperty()
  subgroupId: number;
  @ApiProperty()
  text: string;
}

export class SubgroupType {
  @ApiProperty()
  id: number;
  @ApiProperty()
  groupId: number;
  @ApiProperty()
  text: string;
  @ApiProperty({ type: [WorkType] })
  works: WorkType[];
}

export class HierarchyDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  text: string;
  @ApiProperty({ type: [SubgroupType] })
  subgroups: SubgroupType[];
}
