import { ApiProperty } from '@nestjs/swagger';

export class GroupDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  text: string;
}
