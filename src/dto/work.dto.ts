import { ApiProperty } from '@nestjs/swagger';

export class WorkDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  groupId: number;
  @ApiProperty()
  subgroupId: number;
  @ApiProperty()
  text: string;
}
